import models.Retangle;

public class RectangleClass {
    public static void main(String[] args) throws Exception {
        Retangle retangle1 = new Retangle();
        Retangle retangle2 = new Retangle(3.0f, 2.0f);

        System.out.println("Retangle 1 là: " + retangle1.toString());
        System.out.println("Diện tích của retangle 1 là: " + retangle1.getArea());
        System.out.println("Chu vi của retangle 1 là: " + retangle1.getPerimeter());

        System.out.println("----------------------------------------------------------------");
        System.out.println("Retangle 2 là: " + retangle2.toString());
        System.out.println("Diện tích của retangle 2 là: " + retangle2.getArea());
        System.out.println("Chu vi của retangle 2 là: " + retangle2.getPerimeter());

    }
}
